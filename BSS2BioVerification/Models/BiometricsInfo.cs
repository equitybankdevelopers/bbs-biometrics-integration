﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSS2BioVerification.Models
{
    public class BiometricsInfo
    {
        public CustomerDetails CustomerDetails { get; set; }
        public List<AccountDetail> AccountDetails { get; set; }
        public Status Status { get; set; }
    }
}
