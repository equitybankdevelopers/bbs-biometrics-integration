﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSS2BioVerification.Models
{
    public class Address
    {
        public string LineOne { get; set; }
        public object LineTwo { get; set; }
        public string PostalCode { get; set; }
    }
}
