﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSS2BioVerification.Models
{
    public class Account
    {
        public string BranchID { get; set; }
        public string AccountID { get; set; }
        public string AccountCurrency { get; set; }
        public string SchemeCode { get; set; }
        public string SchemeType { get; set; }
    }
}
