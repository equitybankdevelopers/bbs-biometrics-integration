﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSS2BioVerification.Models
{
    public class Phone
    {
        public string PhoneType { get; set; }
        public string CompleteNumber { get; set; }
        public string LocalNumber { get; set; }
    }
}
