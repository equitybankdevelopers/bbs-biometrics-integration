﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSS2BioVerification.Models
{
    public class CustomerDetails
    {
        public string CustomerID { get; set; }
        public string Fullname { get; set; }
        public string DOB { get; set; }
        public Phone Phone { get; set; }
        public Address Address { get; set; }
    }
}
