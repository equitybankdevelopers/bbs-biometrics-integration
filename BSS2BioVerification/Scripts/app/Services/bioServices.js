﻿app.service('bioService', function ($http, $q) {

    //Get Single Records
    this.getImage = function () {
        return $http.get("/api/Biometrics/");
    }



    //Update the Record
    this.verifyBioData = function (BioData) {
        var request = $http({
            method: "put",
            url: "http://soa3internaldev.ebsafrica.com/ESB/RS/MobileDevice/Rest/Customer/authentication",
            data: BioData,
            contentType: 'application/json'
        });
        console.log("Customer Data" + JSON.stringify(request.toString()));
        return request;
    }

});

//used to set focus on the first input control on a form
app.directive('focusMe', ['$timeout', function ($timeout) {
    return {
        scope: { trigger: '@focusMe' },
        link: function (scope, element) {
            scope.$watch('trigger', function (value) {
                if (value === "true") {
                    $timeout(function () {
                        element[0].focus();
                    });
                }
            });
        }
    };
}]);


