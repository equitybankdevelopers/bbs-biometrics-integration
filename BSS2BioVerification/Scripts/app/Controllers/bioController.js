﻿//bioController
app.controller('bioController', function ($scope, $q, bioService, $timeout, $compile) {
    $scope.searchMode = true;
    $scope.resultMode = false;
    $scope.alertMode = false;

    //capture the print from the Secugen device
    $scope.capturePrint = function () {
        
        var promiseGetSingle = bioService.getImage();

        promiseGetSingle.then(function (pl) {
            var res = pl.data;
            var array = res.split(',');
            if (array[1].length > 1000) {
                $scope.ImageQuality = array[0];
                $scope.ImageData = array[1];
                $scope.DisplayMessage="Fingerprint captured successfully";
                $scope.verifyBioData();
            }
            else{
                $scope.ImageQuality = array[0];
                $scope.ImageData = array[1];
                $scope.DisplayMessage = array[1];
                $scope.resultMode = false;
            }
            $scope.alertMode = true;
            $scope.clear();
        },
        function (errorPl) {
            console.log('Some Error in Getting Details', errorPl);
        });
    }


    //To Create new record and Edit an existing Record.
    $scope.verifyBioData = function () {
        var BioData = {
            Image: $scope.ImageData,
            IdentityDocument: {
                Idnr: $scope.IdNumber,
                IDSerialNr: "string"
            }
        }

        var promisePut = bioService.verifyBioData(BioData);
        promisePut.then(function (pl) {
            var res = pl.data;
            console.log('CustomerInfo', JSON.stringify(res));
            console.log('CustomerInfo', res.Status.Code);
            if (res.Status.Code == "ESB-00") {
                $scope.CustomerNames = res.CustomerDetails.Fullname;
                $scope.AccountDetails = res.AccountDetails[0].Account[0].AccountID;
                $scope.Address = res.CustomerDetails.Address.LineOne + ' - ' + res.CustomerDetails.Address.PostalCode;
                $scope.PhoneNumber = res.CustomerDetails.Phone.LocalNumber;
                $scope.DisplayMessage = res.Status.Description;
                $scope.resultMode = true;
            }
            else {//customer prints don't match those in the DB
                $scope.DisplayMessage = res.Status.Description;
                $scope.resultMode = false
            }
            
            
        }, function (err) {
            console.log("Err" + JSON.stringify(err.toString()));
        });
    }


    //To Clear all Inputs controls value.
    $scope.clear = function () {
        $scope.CustomerNames = "";
        $scope.AccountDetails = "";
        $scope.Address = "";
        $scope.PhoneNumber = "";
    }

$scope.toggleResultMode = function () {
    $scope.resultMode = !$scope.resultMode;
};
});
