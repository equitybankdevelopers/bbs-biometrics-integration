﻿using BSS2BioVerification.Models;
using SecuGen.FDxSDKPro.Windows;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel;
using System.Web.Http;
using System.Web.Http.Cors;
using BSS2BioVerification.BiometricsService;

namespace BSS2BioVerification.API
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BiometricsController : ApiController
    {
        // GET: api/values
        public string GetImage()
        {
            var myBinding = new BasicHttpBinding();
            var myEndpoint = new EndpointAddress("http://localhost:9001/BSS2BioService");
            var myChannelFactory = new ChannelFactory<IBiometrics>(myBinding, myEndpoint);

            IBiometrics client = null;
            string base64 = null;
            try
            {
                client = myChannelFactory.CreateChannel();
                base64 = client.GetImage();
                ((ICommunicationObject)client).Close();
            }
            catch
            {
                if (client != null)
                {
                    ((ICommunicationObject)client).Abort();
                    return null;
                }
            }
            return base64;
        }
    }
}