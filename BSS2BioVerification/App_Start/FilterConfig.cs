﻿using System.Web;
using System.Web.Mvc;

namespace BSS2BioVerification
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
